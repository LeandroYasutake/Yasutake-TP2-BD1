package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"time"

	bolt "github.com/coreos/bbolt"
	_ "github.com/lib/pq"
)

type Cliente struct {
	NroCliente int
	Nombre     string
	Apellido   string
	Direccion  string
	Telefono   string
}

type Comercio struct {
	NroComercio int
	Nombre      string
	Direccion   string
	Cp          int
	Telefono    string
}
type Tarjeta struct {
	IdTarj       int
	Nrotarjeta   int64
	NroCliente   int
	Validadesde  string
	Validahasta  string
	CodSeguridad int
	Limitecompra float64
	Estado       string
}
type Compra struct {
	Nrooperacion int
	Nrotarjeta   int64
	Nrocomercio  int
	Fecha        string
	Monto        int
	Pagado       bool
}

var banderadatabase, banderatablas, banderapkfk, banderainserts bool
var banderafunciones, banderaconsumos, banderacontroltiempo bool
var initBolt, escrituraBolt bool
var respuesta int
var presentacion, presentacion2 string
var menu, menu2, menu3 []string
var opc1, opc2, opc3, opc4, opc5, opc6, opc7, opc8 string
var m2opc1, m2opc2, m2opc3, m2opc4, m2opc5, m2opc6 string
var clientes []Cliente
var tarjetas []Tarjeta
var compras []Compra
var comercios []Comercio

func main() {
	initvar()
	fmt.Printf(presentacion)
	initmenu()
}

func initvar() {
	respuesta = 0
	banderadatabase = false
	banderatablas = false
	banderapkfk = false
	banderainserts = false
	banderafunciones = false
	banderaconsumos = false
	banderacontroltiempo = false
	initBolt = false
	escrituraBolt = false

	presentacion = "\nTrabajo Práctico Nº2 Base de Datos, que desea hacer: \n"
	opc1 = "\n(1) - Crear base de datos tarjeta -\n"
	opc2 = "(2) - Crear tablas para la base de datos -\n"
	opc3 = "(3) - Cargar claves primarias y foráneas en las tablas -\n"
	opc4 = "(4) - Ingresar funciones en la base de datos -\n"
	opc5 = "(5) - Realizar carga de datos en las tablas -\n"
	opc6 = "(6) - Ingresar consumos como compras -\n"
	opc7 = "(7) - Más opciones -\n"
	opc8 = "(8) - Exit -\n\nOprima un número de acuerdo a lo deseado.\n"

	presentacion2 = "\nMás opciones, que desea hacer: \n"
	m2opc1 = "\n(1) - Comenzar control de alertas y rechazos -\n"
	m2opc2 = "(2) - Generar resúmenes -\n"
	m2opc3 = "(3) - Inicializar datos de entidades para JSON/BoltDB -\n"
	m2opc4 = "(4) - Realizar conversión JSON y escritura en base de datos BoltDB -\n"
	m2opc5 = "(5) - Recuperar y mostrar datos de BoldtDB -\n"
	m2opc6 = "(6) - Volver al menú anterior -\n\nOprima un número de acuerdo a lo deseado.\n"

	menu = []string{opc1, opc2, opc3, opc4, opc5, opc6, opc7, opc8}
	menu2 = []string{m2opc1, m2opc2, m2opc3, m2opc4, m2opc5, m2opc6}
}

func initmenu() {
	for _, opcion := range menu {
		fmt.Printf("%s", opcion)
	}

	fmt.Scanf("%d", &respuesta)

	if respuesta == 1 {
		crearbasedatos()
	} else if respuesta == 2 {
		creaciontablas()
	} else if respuesta == 3 {
		cargapkfks()
	} else if respuesta == 4 {
		cargafunciones()
	} else if respuesta == 5 {
		cargainserts()
	} else if respuesta == 6 {
		ingresoconsumos()
	} else if respuesta == 7 {
		masopciones()
	} else if respuesta == 8 {
		fmt.Printf("\n---Programa terminado---\n")
	} else {
		fmt.Printf("\n---Ingrese una opción correcta---\n")
		initmenu()
	}
}

func crearbasedatos() {
	if banderadatabase == false {
		db, err := sql.Open("postgres", "user = postgres dbname = postgres sslmode=disable")
		if err != nil {
			log.Fatal(err)
		}

		_, err = db.Exec(`drop database if exists tarjeta`)
		if err != nil {
			log.Fatal(err)
		}

		_, err = db.Exec(`create database tarjeta`)
		if err != nil {
			log.Fatal(err)
		}

		db.Close()
		banderadatabase = true
		fmt.Printf("\n---Base de datos ha sido creada---\n")
		initmenu()
	} else {
		fmt.Printf("\n---Base de datos ya ha sido creada---\n")
		initmenu()
	}
}

func creaciontablas() {
	if banderatablas == false && banderadatabase == true {
		cargarcomandosql("tablas")
		banderatablas = true
		fmt.Printf("\n---Las tablas han sido creadas---\n")
		initmenu()
	} else if banderadatabase == false {
		fmt.Printf("\n---La base de datos no ha sido creada todavía---\n")
		initmenu()
	} else {
		fmt.Printf("\n---Las tablas ya han sido creadas---\n")
		initmenu()
	}
}

func cargapkfks() {
	if banderapkfk == false && banderatablas == true {
		cargarcomandosql("tablaspkfk")
		banderapkfk = true
		fmt.Printf("\n---Las pk/fk han sido cargadas---\n")
		initmenu()
	} else if banderatablas == false {
		fmt.Printf("\n---Creación de tablas faltante---\n")
		initmenu()
	} else {
		fmt.Printf("\n---Las pk/fk ya han sido cargadas---\n")
		initmenu()
	}
}

func cargainserts() {
	if banderainserts == false && banderapkfk == true {
		cargarcomandosql("inserts")
		banderainserts = true
		fmt.Printf("\n---Los datos han sido cargados---\n")
		initmenu()
	} else if banderapkfk == false {
		fmt.Printf("\n---Creación de pk/fk faltante---\n")
		initmenu()
	} else {
		fmt.Printf("\n---Los datos ya han sido cargados---\n")
		initmenu()
	}
}

func cargafunciones() {
	if banderafunciones == false && banderatablas == true {
		cargarcomandosql("funciones")
		banderafunciones = true
		fmt.Printf("\n---Las funciones han sido cargadas---\n")
		initmenu()
	} else if banderatablas == false {
		fmt.Printf("\n---Creación de tablas faltante---\n")
		initmenu()
	} else {
		fmt.Printf("\n---Las funciones ya han sido cargadas---\n")
		initmenu()
	}
}

func ingresoconsumos() {
	if banderaconsumos == false && banderafunciones == true {
		db, err := sql.Open("postgres", "user = postgres dbname = tarjeta sslmode=disable")
		if err != nil {
			log.Fatal(err)
		}

		_, err = db.Exec(`select ingresarconsumos()`)
		if err != nil {
			log.Fatal(err)
		}

		db.Close()

		banderaconsumos = true
		fmt.Printf("\n---Los consumos han sido ingresados como compras---\n")
		initmenu()
	} else if banderafunciones == false {
		fmt.Printf("\n---Falta carga de datos---\n")
		initmenu()
	} else {
		fmt.Printf("\n---Los consumos ya han sido cargados---\n")
		initmenu()
	}
}

func masopciones() {
	fmt.Printf(presentacion2)
	for _, opcion := range menu2 {
		fmt.Printf("%s", opcion)
	}

	fmt.Scanf("%d", &respuesta)

	if respuesta == 1 {
		controlesportiempo()
	} else if respuesta == 2 {
		generarresumenes()
	} else if respuesta == 3 {
		initJSONBoltVars()
	} else if respuesta == 4 {
		conversionJSONBolt()
	} else if respuesta == 5 {
		imprimirDatosBolt()
	} else if respuesta == 6 {
		initmenu()
	} else {
		fmt.Printf("\n---Ingrese una opción correcta---\n")
		masopciones()
	}
}

func controlesportiempo() {
	if banderaconsumos == true && banderacontroltiempo == false {
		fmt.Printf("\n---Controlando alertas y rechazos por tiempo---\n")
		banderacontroltiempo = true
		go controlarbackground()
		go controlarcompras1min()
		go controlarcompras5min()
		masopciones()
	} else if banderacontroltiempo == true {
		fmt.Printf("\n---Contro de tiempo ya iniciado---\n")
		masopciones()
	} else {
		fmt.Printf("\n---Carga de datos faltante---\n")
		masopciones()
	}
}

func controlarbackground() {
	for {
		time.Sleep(1 * time.Second)
		db, err := sql.Open("postgres", "user = postgres dbname = tarjeta sslmode=disable")
		if err != nil {
			log.Fatal(err)
		}
		_, err = db.Exec(`select alertaporrechazolimite()`)
		if err != nil {
			log.Fatal(err)
		}

		_, err = db.Exec(`select ingresarAlerta()`)
		if err != nil {
			log.Fatal(err)
		}

		time.Sleep(59 * time.Second)
		db.Close()
	}
}

func controlarcompras1min() {
	for {
		db, err := sql.Open("postgres", "user = postgres dbname = tarjeta sslmode=disable")
		if err != nil {
			log.Fatal(err)
		}

		_, err = db.Exec(`select controltiempocompra1min()`)
		if err != nil {
			log.Fatal(err)
		}

		time.Sleep(60 * time.Second)
		db.Close()
	}
}

func controlarcompras5min() {
	for {
		time.Sleep(2 * time.Second)
		db, err := sql.Open("postgres", "user = postgres dbname = tarjeta sslmode=disable")
		if err != nil {
			log.Fatal(err)
		}

		_, err = db.Exec(`select controltiempocompra5min()`)
		if err != nil {
			log.Fatal(err)
		}

		time.Sleep(298 * time.Second)
		db.Close()
	}
}

func generarresumenes() {
	if banderatablas == true {
		fmt.Printf("Ingrese el mes deseado para la facturación:\n")
		fmt.Scanf("%d", &respuesta)
		if respuesta > 0 && respuesta < 13 {
			generarmesfacturado(respuesta)
			fmt.Printf("\n---Resúmenes generados---\n")
			masopciones()
		} else {
			fmt.Printf("\n---Ingrese un mes correcto---\n")
			generarresumenes()
		}
	} else {
		fmt.Printf("\n---Carga de datos faltante---\n")
		masopciones()
	}
}

func generarmesfacturado(mes int) {
	mesfacturado := "select facturarmes(" + strconv.Itoa(mes) + ")"
	db, err := sql.Open("postgres", "user = postgres dbname = tarjeta sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(mesfacturado)
	if err != nil {
		log.Fatal(err)
	}

	db.Close()
}

func cargarcomandosql(tipocomandos string) {
	db, err := sql.Open("postgres", "user = postgres dbname = tarjeta sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(getstringcomandos(tipocomandos))
	if err != nil {
		log.Fatal(err)
	}

	db.Close()
}

func getstringcomandos(tipocomandos string) string {
	comandos, err := ioutil.ReadFile("/home/lilo/Yasutake-TP2-BD1/" + tipocomandos + ".sql")
	if err != nil {
		log.Fatal(err)
	}

	return string(comandos)
}

/* para las pruebas
tarjetas:	(401206605614, 19, 201201, 202009, 8578, 205000.00, 'vigente') sin consumos
			(793911145053, 11, 201810, 202001, 4841, 500000.00, 'suspendida')
			(735670476135, 14, 201812, 201807, 4462, 103000.00, 'suspendida')
			(255266140763, 00, 201010, 201806, 1765, 10000.00, 'anulada')

consumos:	(255266140763, 1765, 16, 355.00) 	anulada por vencimiento
			(107171862314, 2020, 01, 1500.32) 	codigo aut erroneo
			(107171862314, 2020, 13, 3652.00) 	codigo aut erroneo
			(855767686887, 4275, 01, 1500.50) 	supera limite en 2da compra
			(855767686887, 4275, 09, 2500.55) 	supera limite en 2da compra
			(793911145053, 4841, 07, 9866.36) 	suspendida
			(793911145053, 4841, 03, 87582.36) 	suspendida
			(735670476135, 4462, 18, 26539.36) 	suspendida
			(735670476135, 4462, 01, 58366.21) 	suspendida
			(253716535756, 8593, 06, 3653.35) 	supera limite siempre
			(253716535756, 8593, 07, 9856.32) 	supera limite siempre
*/

func initJSONBoltVars() {

	if initBolt == false {
		// clientes
		cliente1 := Cliente{0, "Jeronimo", "Burgos", "J. López 2050", "541146678831"}
		cliente2 := Cliente{1, "Estefania", "Villegas", "Javier García 465", "541144513083"}
		cliente3 := Cliente{2, "Guillermo", "ernandez", "Monseñor 945", "541146671127"}
		clientes = []Cliente{cliente1, cliente2, cliente3}

		// comercios
		comercio1 := Comercio{0, "Mc donalds", "Libertador 7112", 1452, "541148670679"}
		comercio2 := Comercio{1, "Wendys", "Ramallo 985", 1236, "541147492388"}
		comercio3 := Comercio{5, "KFC", "Av. Callao 131", 1425, "541147963055"}
		comercios = []Comercio{comercio1, comercio2, comercio3}

		// tarjetas
		tarjeta1 := Tarjeta{1, 255266140763, 0, "201010", "201806", 1765, 10000.0, "anulada"}
		tarjeta2 := Tarjeta{2, 276593831213, 0, "201011", "201908", 2952, 22000.0, "vigente"}
		tarjeta3 := Tarjeta{3, 735670476135, 14, "201812", "201807", 4462, 103000.00, "suspendida"}
		tarjetas = []Tarjeta{tarjeta1, tarjeta2, tarjeta3}

		// compras
		compra1 := Compra{0, 255266140763, 0, "2018-05-19 21:09:08", 1000, true}
		compra2 := Compra{1, 276593831213, 1, "2018-05-21 19:09:08", 5000, true}
		compra3 := Compra{2, 735670476135, 5, "2018-04-19 13:09:08", 1000, true}
		compras = []Compra{compra1, compra2, compra3}

		fmt.Printf("\n---Inicialización de variables realizada---\n")
		initBolt = true
		masopciones()
	} else {
		fmt.Printf("\n---Inicialización de variables ya realizada---\n")
		masopciones()
	}
}

func conversionJSONBolt() {
	if initBolt == true && escrituraBolt == false {
		escrituraBolt = true

		db, err := bolt.Open("tp2bd1.db", 0600, nil)
		if err != nil {
			log.Fatal(err)
		}

		for _, cliente := range clientes {
			data, err := json.Marshal(cliente)
			if err != nil {
				log.Fatal(err)
			}

			createUpdate(db, "cliente", []byte(strconv.Itoa(cliente.NroCliente)), data)
		}

		for _, comercio := range comercios {
			data, err := json.Marshal(comercio)
			if err != nil {
				log.Fatal(err)
			}

			createUpdate(db, "comercio", []byte(strconv.Itoa(comercio.NroComercio)), data)
		}

		for _, tarjeta := range tarjetas {
			data, err := json.Marshal(tarjeta)
			if err != nil {
				log.Fatal(err)
			}

			createUpdate(db, "tarjeta", []byte(strconv.Itoa(tarjeta.IdTarj)), data)
		}

		for _, compra := range compras {
			data, err := json.Marshal(compra)
			if err != nil {
				log.Fatal(err)
			}

			createUpdate(db, "compra", []byte(strconv.Itoa(compra.Nrooperacion)), data)
		}

		db.Close()
		fmt.Printf("\n---Conversión JSON y escritura en BoltDB realizada---\n")
		masopciones()
	} else if escrituraBolt == true {
		fmt.Printf("\n---Conversión JSON y escritura en BoltDB ya ha sido realizada---\n")
		masopciones()
	} else {
		fmt.Printf("\n---Inicialización de variables faltante---\n")
		masopciones()
	}
}

func imprimirDatosBolt() {
	if escrituraBolt == true {
		db, err := bolt.Open("tp2bd1.db", 0600, nil)
		if err != nil {
			log.Fatal(err)
		}

		for _, cliente := range clientes {
			resultado, err := readUnique(db, "cliente", []byte(strconv.Itoa(cliente.NroCliente)))
			if err != nil {
				log.Fatal(err)
			}
			fmt.Printf("%s\n", resultado)
		}

		for _, comercio := range comercios {
			resultado1, err := readUnique(db, "comercio", []byte(strconv.Itoa(comercio.NroComercio)))
			if err != nil {
				log.Fatal(err)
			}
			fmt.Printf("%s\n", resultado1)
		}

		for _, tarjeta := range tarjetas {
			resultado2, err := readUnique(db, "tarjeta", []byte(strconv.Itoa(tarjeta.IdTarj)))
			if err != nil {
				log.Fatal(err)
			}
			fmt.Printf("%s\n", resultado2)
		}

		for _, compra := range compras {
			resultado3, err := readUnique(db, "compra", []byte(strconv.Itoa(compra.Nrooperacion)))
			if err != nil {
				log.Fatal(err)
			}
			fmt.Printf("%s\n", resultado3)
		}
		db.Close()
		masopciones()
	} else {
		fmt.Printf("\n---Escritura de variables faltante---\n")
		masopciones()
	}
}

func createUpdate(db *bolt.DB, bucketName string, key []byte, val []byte) error {
	// abre transaccion de escritura
	tx, err := db.Begin(true)
	if err != nil {
		return err
	}
	defer tx.Rollback()
	b, _ := tx.CreateBucketIfNotExists([]byte(bucketName))
	err = b.Put(key, val)
	if err != nil {
		return err
	}
	// cierra transacción
	if err := tx.Commit(); err != nil {
		return err
	}
	return nil
}

func readUnique(db *bolt.DB, bucketName string, key []byte) ([]byte, error) {
	var buf []byte
	// abre una transacción de lectura
	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucketName))
		buf = b.Get(key)
		return nil
	})
	return buf, err
}
