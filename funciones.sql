-----------funcion para ingresar como compras a los consumos------------
create or replace function 
ingresarconsumos()
returns void as $$
declare
    c_row consumo%rowtype;
begin
    FOR c_row in SELECT * from consumo loop
        perform ingresarcompra(c_row.nrotarjeta, c_row.codseguridad, c_row.nrocomercio, c_row.monto);
    END loop;
end;
$$ language plpgsql;


--------------utiliza autorizarcompra() para ingresar datos en la tabla compra
create or replace function 
ingresarcompra(numtarjeta char, codigoseg char, numcomercio int, montocompra decimal)
returns void as $$
declare
    numOperacion int;
    autorizado boolean;
begin
    select autorizarcompra(numtarjeta, codigoseg, numcomercio, montocompra) into autorizado;
    if autorizado = TRUE THEN
        select (MAX(nrooperacion) + 1) into numOperacion from compra;
        if numOperacion is null THEN
            numOperacion = 1;
        end if;
        insert into compra values(
                numOperacion,
                numtarjeta,
                numcomercio,
                current_timestamp,
                montocompra,
                FALSE); 
    end if;
end;
$$ language plpgsql;


-------------------autoriza o no las compras segun sus datos
create or replace function 
autorizarcompra(numtarjeta char, codigoseg char, numcomercio int, montocompra decimal)
returns BOOLEAN as $$
declare
    numrechazo int;
    numOperacion int;
    montotal decimal;
    limiteTarjeta decimal;
    fvenc char(6);
    estadoactual char(10);
begin

---------------tarjeta inexistente----------------------------------------
    perform * from tarjeta where nrotarjeta = numtarjeta;
    if not found then 
        select (MAX(nrorechazo) + 1) into numrechazo from rechazo;
        if numrechazo is null THEN
        numrechazo = 0;
        end if;
        insert into rechazo values(
            numrechazo,
            numtarjeta,
            numcomercio,
            current_timestamp, 
            montocompra,
            'Tarjeta no existente');
        return false;  
    end if; 

    ---------------codigo seg invalido------------------------------------------
    perform * from tarjeta where nrotarjeta = numtarjeta and codseguridad = codigoseg;
    if not found then
        select (MAX(nrorechazo) + 1) into numrechazo from rechazo;
         if numrechazo is null THEN
        numrechazo = 0;
        end if;

        insert into rechazo values(
            numrechazo,
            numtarjeta,
            numcomercio,
            current_timestamp, 
            montocompra,
            'código de seguridad inválido');
        return false;  
    end if;

---------------limite excedido--------------------------------------
    select sum(monto) into montotal from compra where numtarjeta = nrotarjeta;
    if montotal is null THEN
        montotal = 0;
        end if;
    select limitecompra into limiteTarjeta from tarjeta where numtarjeta = nrotarjeta;
    if montotal + montocompra > limiteTarjeta then
        select (MAX(nrorechazo) + 1) into numrechazo from rechazo;
        if numrechazo is null THEN
        numrechazo = 0;
        end if;
        insert into rechazo values(
            numrechazo,
            numtarjeta,
            numcomercio,
            current_timestamp, 
            montocompra,
            'supera límite de tarjeta');
        return false;  
    end if;

    --------------vigencia expirado-------------------------------------------
    select validahasta into fvenc from tarjeta where numtarjeta = nrotarjeta;
    if to_date(fvenc, 'YYYYMM') <= now()::date THEN
        select (MAX(nrorechazo) + 1) into numrechazo from rechazo;
         if numrechazo is null THEN
        numrechazo = 0;
        end if;
        insert into rechazo values(
            numrechazo,
            numtarjeta,
            numcomercio,
            current_timestamp, 
            montocompra,
            'plazo de vigencia expirado');
        return false;  
    end if;

    --------------Tarjeta suspendida---------------------------------------------
    select estado into estadoactual from tarjeta where numtarjeta = nrotarjeta;
    if estadoactual = 'suspendida' THEN
        select (MAX(nrorechazo) + 1) into numrechazo from rechazo;
        if numrechazo is null THEN
            numrechazo = 0;
        end if;
        insert into rechazo values(
            numrechazo,
            numtarjeta,
            numcomercio,
            current_timestamp, 
            montocompra,
            'la tarjeta se encuentra suspendida');
        return false;  
    end if; 

return true;
end;
$$ language plpgsql;


--------------generar resumen por mes
create or replace function 
facturarmes(mes INT)
returns void as $$
declare
    c_row cliente%rowtype;
begin
    FOR c_row in SELECT * from cliente loop
        perform generarResumen(c_row.nrocliente, mes, 2018);
    END loop;
end;
$$ language plpgsql;

------------------------------------------------------------------------------------
---------------------Funcion Generadora del resumen---------------------------------
------------------------------------------------------------------------------------

--La funcion recibe como parametros un numero de cliente y un año que es el mes
--del resumen. La funcion debe insertar correctamente en las tablas cabecera
--y detalle los parametros: nroresumen, nombre, apellido, domicilio, nrotarjeta, 
--desde, hasta y vence total para la tabla cabecera y para el detalle los parametros
--nroresumen, nrolinea, fecha, nombrecomercio y el monto(de cada consumo) para cada tarjeta.

create or replace function 
generarResumen(numcliente int, mesperiodo int, añoperiodo int) 
returns void as $$
DECLARE
    numresumen int;
    numlinea int;
	nombreaux text;
	apellidoaux text;
	domicilioaux text;
	nrotarj char(12);
	desdeaux date;
	hastaaux date;
	venceaux date;
	totalResumen decimal(8,2);
    totalResumenParcial decimal(8,2);
    nrolinea int;
    c_row compra%rowtype;
    t_row tarjeta%rowtype;

BEGIN
    totalResumen = 0;

    select nrotarjeta into nrotarj from tarjeta where nrocliente = numcliente;
        
    SELECT nombre into nombreaux FROM cliente WHERE nrocliente = numcliente;
    SELECT apellido into apellidoaux FROM cliente WHERE nrocliente = numcliente;
    SELECT domicilio into domicilioaux FROM cliente WHERE nrocliente = numcliente;

    for t_row in SELECT * from tarjeta where nrocliente = numcliente loop
        select SUM(monto) into totalResumenParcial from compra 
        where t_row.nrotarjeta = compra.nrotarjeta and
        EXTRACT(YEAR FROM compra.fecha) = añoperiodo and EXTRACT(MONTH FROM compra.fecha) = mesperiodo;

        if totalResumenParcial is NULL THEN
            totalResumenParcial = 0;
        end if;

        totalresumen = totalresumen + totalResumenParcial;

        SELECT fechainicio into desdeaux from cierre 
        where cierre.terminacion = right(t_row.nrotarjeta, 1)::int and cierre.mes = mesperiodo and cierre.año = añoperiodo; 

        SELECT fechacierre into hastaaux from cierre 
        where cierre.terminacion = right(t_row.nrotarjeta, 1)::int and cierre.mes = mesperiodo and cierre.año = añoperiodo;

        SELECT fechavto into venceaux from cierre 
        where cierre.terminacion = right(t_row.nrotarjeta, 1)::int and cierre.mes = mesperiodo and cierre.año = añoperiodo; 
        
        select (MAX(nroresumen) + 1) into numresumen from cabecera;
        if numresumen is null THEN
            numresumen = 0;
        end if;
       
        INSERT INTO cabecera VALUES(numresumen, nombreaux, apellidoaux, domicilioaux, 
        t_row.nrotarjeta, desdeaux, hastaaux,  venceaux, totalResumen);
        
        FOR c_row in SELECT * from compra where t_row.nrotarjeta = compra.nrotarjeta loop
            if EXTRACT(YEAR FROM c_row.fecha) = añoperiodo and EXTRACT(MONTH FROM c_row.fecha) = mesperiodo THEN
                select (MAX(detalle.nrolinea) + 1) into numlinea from detalle;
                if numlinea is null THEN
                    numlinea = 0;
                end if;
                INSERT INTO detalle VALUES(numresumen, numlinea, c_row.fecha, c_row.nrocomercio, c_row.monto);
            END IF;
        END loop;
        update compra set pagado = true where t_row.nrotarjeta = compra.nrotarjeta;    
    end loop;
end;
$$ language plpgsql;
-----------------------------------------------------

--------------ingresar los rechazos en alertas
create or replace function ingresarAlerta() returns void as $$
declare
    r_row rechazo%rowtype;
    numalerta int;
    codigorechazo int;
    descrip text;
begin
    codigorechazo := 0;

    for r_row in SELECT * from rechazo loop
        perform * from alerta where r_row.nrorechazo = alerta.nrorechazo and r_row.motivo = alerta.descripcion;
        if NOT FOUND THEN
            if r_row.motivo = 'supera límite de tarjeta' then
                codigorechazo = 32;
            end if;

            select (MAX(nroalerta) + 1) into numalerta from alerta;
            if numalerta is null THEN
                numalerta = 0;
            end if; 

            insert into alerta values (
                numalerta,
                r_row.nrotarjeta,
                r_row.fecha,
                r_row.nrorechazo,
                codigorechazo,
                r_row.motivo
            );
        END IF;
    end loop;
end;
$$ language plpgsql;
-----------------------------------------------------------

-----------------alerta por limite dos rechazos------------
create or replace function alertaporrechazolimite() returns void as $$
declare
    r1_row rechazo%rowtype;
    r2_row rechazo%rowtype;
    cantrechazos int;
    numalerta int;
begin
    for r1_row in SELECT DISTINCT ON (nrotarjeta) * from rechazo where motivo = 'supera límite de tarjeta' loop
        cantrechazos = 0;
        
        for r2_row in SELECT * from rechazo where motivo = 'supera límite de tarjeta' loop 
            if r1_row.nrotarjeta = r2_row.nrotarjeta and r1_row.fecha::date = r2_row.fecha::date then
                cantrechazos = cantrechazos + 1;
            end if;
        end loop;

        if cantrechazos > 1 THEN
            select (MAX(nroalerta) + 1) into numalerta from alerta;
            if numalerta is null THEN
                numalerta = 0;
            end if;

            IF NOT EXISTS (SELECT 1 FROM alerta where nrotarjeta = r1_row.nrotarjeta 
            and fecha = r1_row.fecha and r1_row.nrorechazo = nrorechazo 
            and codalerta = 32 and descripcion = 'Se detectaron dos rechazos por limite durante el mismo dia') THEN  --solo puedo suspender a la tarjeta 1 vez al dia

                insert into alerta values(
                    numalerta, 
                    r1_row.nrotarjeta,
                    r1_row.fecha, 
                    r1_row.nrorechazo, 
                    32,
                    'Se detectaron dos rechazos por limite durante el mismo dia'
                );

                update tarjeta set estado = 'suspendida' where nrotarjeta = r1_row.nrotarjeta;
            end if;
        end if;
    end loop;
end;
$$ language plpgsql;
------------------------------------------------


-------------controla compras en comercios con iguales o distintos codigos postales
-------------antes como a trigger, ahora como funcion. se le pasa un int que informa
-------------que cant de tiempo es la deseada a controlar y la funcion alerta si hubo
-------------compras con esas caracteristicas

create or replace function 
controltiempocompra1min()
returns void as $$
declare
    c_row compra%rowtype;
    c2_row compra%rowtype;
    cp char(8);
    cp2 char(8);
    numalerta int;
    descripaux text;
begin
    descripaux = 'conflicto en compra < 1 min';
    
    FOR c_row in SELECT * from compra order by fecha desc loop  --eliminamos los comercios iguales ya que no nos interesan para la misma tarjeta
        
        FOR c2_row in SELECT * from compra order by fecha loop
            
            if c_row.nrotarjeta = c2_row.nrotarjeta and c_row.nrooperacion != c2_row.nrooperacion and 
                EXTRACT(EPOCH FROM (c_row.fecha - c2_row.fecha)) < 600 THEN
                
                select codigopostal into cp from comercio where c_row .nrocomercio = comercio.nrocomercio;
                select codigopostal into cp2 from comercio where c2_row.nrocomercio = comercio.nrocomercio;

                if cp = cp2 and c_row.nrocomercio != c2_row.nrocomercio THEN --encontre compras con cp's iguales pero comercios !=
                    select (MAX(nroalerta) + 1) into numalerta from alerta;
                    if numalerta is null THEN
                        numalerta = 0;
                    end if;

                    perform * from alerta where nrotarjeta = c_row.nrotarjeta and fecha = c_row.fecha
                    and descripcion = descripaux; --elimina alertas repetidas
                   
                    if not FOUND THEN
                    insert into alerta values(
                        numalerta, 
                        c_row.nrotarjeta,
                        c_row.fecha, 
                        NULL, 
                        1,
                        descripaux
                    );
                    end if;
                END if;
            end if;
        END loop;
    END loop;
end;
$$ language plpgsql;
--------------------------------------

create or replace function 
controltiempocompra5min()
returns void as $$
declare
    c_row compra%rowtype;
    c2_row compra%rowtype;
    cp char(8);
    cp2 char(8);
    numalerta int;
    descripaux text;
begin
    descripaux = 'conflicto en compra < 5 min';
    
    FOR c_row in SELECT * from compra order by fecha desc loop  --eliminamos los comercios iguales ya que no nos interesan para la misma tarjeta
        
        FOR c2_row in SELECT * from compra order by fecha loop
            
            if c_row.nrotarjeta = c2_row.nrotarjeta and c_row.nrooperacion != c2_row.nrooperacion 
                and EXTRACT(EPOCH FROM (c_row.fecha - c2_row.fecha)) < 700 THEN --evito compras viejas
                
                select codigopostal into cp from comercio where c_row .nrocomercio = comercio.nrocomercio;
                select codigopostal into cp2 from comercio where c2_row.nrocomercio = comercio.nrocomercio;

                IF cp != cp2 THEN  --compras con la misma tarjeta en cp's distintos en 5 min
                    select (MAX(nroalerta) + 1) into numalerta from alerta;
                    if numalerta is null THEN
                        numalerta = 0;
                    end if;

                    perform * from alerta where nrotarjeta = c_row.nrotarjeta and fecha = c_row.fecha
                    and descripcion = descripaux; --elimina alertas repetidas
                    
                    if not FOUND THEN
                    insert into alerta values(
                        numalerta, 
                        c_row.nrotarjeta,
                        c_row.fecha, 
                        NULL, 
                        1,
                        descripaux
                    );
                    end if;    
                end if;
            end if;
        END loop;
    END loop;
end;
$$ language plpgsql;
--------------------------------------



-----insert into alerta frmo rechazo como trigger

/*create or replace function ingresarAlerta() returns trigger as $$
declare
    numalerta int;
    codigorechazo int;
    descrip text;
begin
    codigorechazo := 0;
    if new.motivo = 'supera límite de tarjeta' then
        codigorechazo = 32;
    end if; 

    select (MAX(nroalerta) + 1) into numalerta from alerta;
    if numalerta is null THEN
        numalerta = 0;
    end if;
    
    insert into alerta values(
        numalerta, 
        new.nrotarjeta,
        new.fecha, 
        new.nrorechazo, 
        codigorechazo,
        new.motivo
    );
    return new;
end;
$$ language plpgsql;


create trigger ingresarAlerta
after insert on rechazo
for each row
execute procedure ingresarAlerta();*/ 
-------------------------------------------------

-----------------alerta por dos rechazos por limite como trigger------------
/*create or replace function alertaporrechazo() returns trigger as $$
declare
    cantrechazos int;
    numalerta int;
begin
    select count(*) into cantrechazos from rechazo where new.nrotarjeta = rechazo.nrotarjeta 
    and motivo = 'supera límite de tarjeta' and rechazo.fecha::date = new.fecha::date;

    if cantrechazos >= 2 then
        select (MAX(nroalerta) + 1) into numalerta from alerta;
            if numalerta is null THEN
                numalerta = 0;
            end if;
    
            insert into alerta values(
                numalerta, 
                new.nrotarjeta,
                new.fecha, 
                new.nrorechazo, 
                32,
                'Se detectaron dos rechazos por limite durante el mismo dia'
            );

            update tarjeta set estado = 'suspendida' where nrotarjeta = new.nrotarjeta;
    end if;
    return new;
end;
$$ language plpgsql;

create trigger alertaporrechazo
after insert on rechazo
for each row
execute procedure alertaporrechazo();*/
----------------------------------------------------------


------------------compra en menos de 1 min y 5-------------------------   como trigger
/*create or replace function controltiempocompra() returns trigger as $$
declare
    ultimacompra timestamp;
    numcomercio int;
    cpnew char(8);
    cpold char(8);
    numalerta int;
begin
    select MAX(fecha) into ultimacompra from compra where compra.nrotarjeta = new.nrotarjeta;
    
    select nrocomercio into numcomercio from compra where compra.nrotarjeta = new.nrotarjeta and
    ultimacompra = compra.fecha;
    
    select codigopostal into cpnew from comercio where new.nrocomercio = nrocomercio;
    select codigopostal into cpold from comercio where numcomercio = nrocomercio;

    if EXTRACT(EPOCH FROM (new.fecha - ultimacompra)) <= 60 and 
        numcomercio != new.nrocomercio and
        cpnew = cpold then
        
        select (MAX(nroalerta) + 1) into numalerta from alerta;
        if numalerta is null THEN
            numalerta = 0;
        end if;
    
        insert into alerta values(
            numalerta, 
            new.nrotarjeta,
            new.fecha, 
            NULL, 
            1,
            'conflicto en compra < 1 min'
    );
    end if;

    if EXTRACT(EPOCH FROM (new.fecha - ultimacompra)) <= 300 and 
        numcomercio != new.nrocomercio and
        cpnew != cpold then
        
        select (MAX(nroalerta) + 1) into numalerta from alerta;
        if numalerta is null THEN
            numalerta = 0;
        end if;
    
        insert into alerta values(
            numalerta, 
            new.nrotarjeta,
            new.fecha, 
            NULL, 
            1,
            'conflicto en compra < 5 min en comercios distintos'
        );
    end if;
    return new;
end;
$$ language plpgsql;

create trigger controltiempocompra
BEFORE insert on compra
for each row
execute procedure controltiempocompra();*/
-------------------------------------------------------------------------------
