package main

import (
	"fmt"
)

func main() {
	var diainicio int
	var diacierre int
	var diavto int
	año := 2018

	for mes := 1; mes < 13; mes++ {
		for terminacion := 0; terminacion < 10; terminacion++ {
			mescierre := mes
			mesvto := mes
			if terminacion == 0 {
				diainicio = 01
				diacierre = 01
				diavto = 20
				if mes == 12 {
					mescierre = 01
					mesvto = mescierre
					año = 2019
				} else {
					mescierre = mes + 1
					mesvto = mescierre
				}
			} else if terminacion == 1 {
				diainicio = 03
				diacierre = 03
				diavto = 23
				if mes == 12 {
					mescierre = 01
					mesvto = mescierre
					año = 2019
				} else {
					mescierre = mes + 1
					mesvto = mescierre
				}
			} else if terminacion == 2 {
				diainicio = 05
				diacierre = 05
				diavto = 25
				if mes == 12 {
					mescierre = 01
					mesvto = mescierre
					año = 2019
				} else {
					mescierre = mes + 1
					mesvto = mescierre
				}
			} else if terminacion == 3 {
				diainicio = 07
				diacierre = 07
				diavto = 27
				if mes == 12 {
					mescierre = 01
					mesvto = mescierre
					año = 2019
				} else {
					mescierre = mes + 1
					mesvto = mescierre
				}
			} else if terminacion == 4 {
				diainicio = 10
				diacierre = 10
				diavto = 30
				if mes == 12 {
					mescierre = 01
					mesvto = mescierre
					año = 2019
				} else if mes == 01 {
					mescierre = mes + 1
					mesvto = mescierre
					diavto = 28
				} else {
					mescierre = mes + 1
					mesvto = mescierre
				}
			} else if terminacion == 5 {
				diainicio = 11
				diacierre = 11
				diavto = 30
				if mes == 12 {
					mescierre = 01
					mesvto = mescierre
					año = 2019
				} else if mes == 01 {
					mescierre = mes + 1
					mesvto = mescierre
					diavto = 28
				} else {
					mescierre = mes + 1
					mesvto = mescierre
				}
			} else if terminacion == 6 {
				diainicio = 13
				diacierre = 13
				diavto = 03
				if mes == 12 {
					mescierre = 01
					mesvto = mescierre + 1
					año = 2019
				} else if mes == 11 {
					mescierre = mes + 1
					mesvto = 01
					año = 2019
				} else {
					mescierre = mes + 1
					mesvto = mescierre + 1
				}
			} else if terminacion == 7 {
				diainicio = 15
				diacierre = 15
				diavto = 05
				if mes == 12 {
					mescierre = 01
					mesvto = mescierre + 1
					año = 2019
				} else if mes == 11 {
					mescierre = mes + 1
					mesvto = 01
					año = 2019
				} else {
					mescierre = mes + 1
					mesvto = mescierre + 1
				}
			} else if terminacion == 8 {
				diainicio = 17
				diacierre = 17
				diavto = 07
				if mes == 12 {
					mescierre = 01
					mesvto = mescierre + 1
					año = 2019
				} else if mes == 11 {
					mescierre = mes + 1
					mesvto = 01
				} else {
					mescierre = mes + 1
					mesvto = mescierre + 1
				}
			} else {
				diainicio = 19
				diacierre = 19
				diavto = 10
				if mes == 12 {
					mescierre = 01
					mesvto = mescierre + 1
					año = 2019
				} else if mes == 11 {
					mescierre = mes + 1
					mesvto = 01
					año = 2019
				} else {
					mescierre = mes + 1
					mesvto = mescierre + 1
				}
			}

			fmt.Printf("insert into cierre values (2018, %d, %d,'2018-%d-%d','%d-%d-%d','%d-%d-%d');\n", mes, terminacion, mes, diainicio, año, mescierre, diacierre, año, mesvto, diavto)
		}
	}

}
